import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { Row, Col, Card } from "react-bootstrap";
import Link from "next/link";
//to redirect the user, use the Router component from next JS
import Router from "next/router";
import Image from "next/image";
import UserContext from "../../userContext";
//import the google login component from react google log in
import { GoogleLogin } from "react-google-login";

export default function Login() {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	//conditionl rendering for button
	const [isActive, setIsActive] = useState(true);
	//Destructure
	const { user, setUser } = useContext(UserContext);
	//use effect to monitor any changes made to email pass
	useEffect(() => {
		if (email !== "" && password !== "") {
		setIsActive(true);
		} else {
		setIsActive(false);
		}
	}, [email, password]);
	function authenticate(e) {
		e.preventDefault();
		fetch("https://young-springs-92350.herokuapp.com/api/users/login", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			email: email,
			password: password,
		}),
		})
		.then((res) => res.json())
		.then((data) => {
			//since may token na nakukuha gumawa condition to save sa localstorage
			if (data.accessToken) {
			localStorage.setItem("token", data.accessToken);
			//gumamit muli ng fetch para makuha ang details ng user
			fetch("https://young-springs-92350.herokuapp.com/api/users/details", {
				//di kelangan ng method since get to
				headers: {
				Authorization: `Bearer ${data.accessToken}`,
				},
			})
				.then((res) => res.json())
				.then((data) => {
				localStorage.setItem("email", data.email),
					localStorage.setItem("id", data._id);
				//after getting the users details from the API derver we will set the global uesr state
				setUser({
					email: data.email,
					id: data._id,
				});
				});
			Swal.fire({
				icon: "success",
				title: "Successful Log In",
				text: "Thank you for logging in",
			});
			//router component ito na mag reredirect sa user sa endpoint given as an argument dun sa method
			Router.push("/");
			} else {
			Swal.fire({
				icon: "error",
				title: "Log In Failed",
				text: "Please Check your inputs",
			});
			}
		});
		//------------------End of authentication function-----------------
		//set the input states into their initial value
		setEmail("");
		setPassword("");
	}
	//function para sa onsuccess and on failure for the google login
	function authenticateGoogleToken(response) {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/verify-google-id-token",
		{
			method: "POST",
			headers: {
			"Content-Type": "application/json",
			},
			body: JSON.stringify({
			tokenId: response.tokenId,
			accessToken: response.accessToken,
			}),
		}
		)
		.then((res) => res.json())
		.then((data) => {
			//we will show alerts to show if the user logged in properly or if there are errors
			if (typeof data.accessToken !== "undefined") {
			//set our accessToken in to our localStorage as token:
			localStorage.setItem("token", data.accessToken);
			//run a fetch request to get our users details and update our global user state and save our user details into the localStorage:
			fetch("https://young-springs-92350.herokuapp.com/api/users/details", {
				headers: {
				Authorization: `Bearer ${data.accessToken}`,
				},
			})
				.then((res) => res.json())
				.then((data) => {
				localStorage.setItem("email", data.email);
				localStorage.setItem("isAdmin", data.isAdmin);
				//after getting the user'd details, update the global user state
				setUser({
					email: data.email,
					isAdmin: data.isAdmin,
				});
				//Fire a sweetalert to inform the user of successfull login
				Swal.fire({
					icon: "success",
					title: "Successful Login",
				});
				Router.push("/transactions");
				});
			} else {
			//if data.accessToken is undefined, therefore data contans a property called erro instread
			if (data.error === "google-auth-error") {
				Swal.fire({
				icon: "error",
				title: "Google Authentication Failed",
				});
			} else if (data.error === "login-type-error") {
				Swal.fire({
				icon: "error",
				title: "Login failed",
				text: "You may have registered through a different procedure",
				});
			}
			}
		});
	}
	/*
	Google Login Button - a react component used for google login through its package: react-google-login
	clientId = OAuthClient id from Cloud Google Developers Platform used for authorizing the use of Google API and Google OAuthClient.
	onSuccess = runs a function which returns a GoogleUser object which provides access to all of the GoogleUser methods and details.
	onFailure = a function is called or ran when either initialization or a signin attempt fails.
	cookiePolicy = determines cookie policy for the origin of the google login requests.
	buttonText = changeable/mutable string for the google login button text.
	*/
	return (
		<div className="login-form">
			<Row className="my-4">
				<Col md={12}>
					<Card className="my-4 cardlogin">
						<div className="budget-logo">
							<Image
								src="/money.png"
								alt="Picture of the author"
								width={200}
								height={200}
								className="text-center"
							/>
						</div>
					<h2 className="my-4 about-title text-center">Budget ni Juan</h2>
					<Link href="/transactions/aboutApp">
						<a className="text-center about-desc" role="button">
						<h5 className="about-app">Learn More</h5>
						</a>
					</Link>
					<Form className="mx-4 my-4" onSubmit={(e) => authenticate(e)}>
						<Form.Group controlId="userEmail">
						<Form.Label className="login-label">Email:</Form.Label>
						<Form.Control
							autoComplete="off"
							className="logdetails"
							type="email"
							placeholder="Enter Email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
							required
						/>
						</Form.Group>
						<Form.Group controlId="userPassword">
						<Form.Label className="logdetails" className="login-label">
							Password:
						</Form.Label>
						<Form.Control
							autoComplete="off"
							type="password"
							placeholder="Enter Password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							required
						/>
						</Form.Group>
						{isActive ? (
						<Button type="submit" className="btn-block">
							LogIn
						</Button>
						) : (
						<Button variant="dark" disabled className="btn-block">
							LogIn
						</Button>
						)}
						<GoogleLogin
						className="mx-4"
						clientId="745766178887-r0hvincc68j0cjevl597s3aa5vgjluqu.apps.googleusercontent.com"
						buttontext="Login Using Google"
						cookiePolicy={"single_host_origin"}
						onSuccess={authenticateGoogleToken}
						onFailure={authenticateGoogleToken}
						className="w-100 text-center my-4 d-flex justify-content-center"
						/>
					</Form>
					<Link href="/register">
						<a className="my-4 text-center" role="button">
						<h5 className="about-app">Ready to track your Budget? Register!</h5>
						</a>
					</Link>
					</Card>
				</Col>
			</Row>
		</div>	
	);
}
