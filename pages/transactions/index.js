import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../../userContext";
import { Row, Col, Card } from "react-bootstrap";
import Swal from "sweetalert2";
import Filters from './filter';
import Moment from "react-moment";
import Profile from './profile';

export default function Transactions() {
  //destructure the returned value of our context
	const { user } = useContext(UserContext);
	//variable to capitalize
	//variable to get the user details
	const [userFirstName, setUserFirstName] = useState([]);
	const [userLastName, setUserLastName] = useState([]);
	//Variables for fetching options for Category and records
	const [records, setRecords] = useState([]);
	//Variables For adding category (useStates)
	const [categoryName, setCategoryName] = useState("");
	const [categoryType, setCategoryType] = useState("");
	//Variables For adding record (useStates)
	const [type, setType] = useState("");
	const [categoryTypeRecord, setCategoryTypeRecord] = useState("");
	const [descriptionRecord, setDescriptionRecord] = useState("");
	const [amountRecord, setAmountRecord] = useState(0);
	const [userToken, setUserToken] = useState({ userToken: null });
	const [incExpType, setIncExpType] = useState("");
	const [categoryOption, setCategoryOption] = useState([]);
	const [categoryIncome, setCategoryIncome] = useState([]);
	const [categoryExpense, setCategoryExpense] = useState([]);
	const [addedCategory, setAddedCategory] = useState(false);
	const [addedTransaction, setAddedTransaction] = useState(false);
	const [addedRec, setAddedRec] = useState(false);
	//For Buttons
	const [isActive1, setIsActive1] = useState(true);
	const [isActive2, setIsActive2] = useState(true);
	//To monitor if textbox are not blank in adding category
	useEffect(() => {
		if (categoryName !== "" && categoryType !== "") {
		setIsActive1(true);
		} else {
		setIsActive1(false);
		}
	}, [categoryName, categoryType]);
	//To monitor if textbox are not blank in adding record
	useEffect(() => {
		if (
		type !== "" &&
		categoryTypeRecord !== "" &&
		descriptionRecord !== "" &&
		amountRecord !== ""
		) {
		setIsActive2(true);
		} else {
		setIsActive2(false);
		}
	}, [type, categoryTypeRecord, descriptionRecord, amountRecord]);
	//use effect for user details
	useEffect(() => {
		fetch("https://young-springs-92350.herokuapp.com/api/users/details", {
		headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
		},
		})
		.then((res) => res.json())
		.then((data) => {
			setUserFirstName(data.firstName);
			setUserLastName(data.lastName);
		});
	}, []);
	//use effect for all categories
	useEffect(() => {
		fetch("https://young-springs-92350.herokuapp.com/api/users/allCategories", {
		headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
		},
		})
		.then((res) => res.json())
		.then((data) => {
			setCategoryOption(data);
			let categoryInc = data.filter((item) => item.type === "Income");
			setCategoryIncome(categoryInc);
			let categoryExp = data.filter((item) => item.type === "Expense");
			setCategoryExpense(categoryExp);
		});
	}, [addedCategory]);
	const categoryIncomeOption = categoryIncome.map((category) => {
		return <option key={category._id}>{category.name}</option>;
	});
	const categoryExpenseOption = categoryExpense.map((category) => {
		return <option key={category._id}>{category.name}</option>;
	});
	//use effect for all Records
	useEffect(() => {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
		{
			headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		}
		)
		.then((res) => res.json())
		.then((data) => {
			setRecords(data);
		});
	}, [addedTransaction]);
	const allRecords = records.map((record) => {
		return (
		<Card key={record._id}>
			<Card.Body>
			<Card.Title className="card_title">{record.category}</Card.Title>
			<Card.Text className="card_title">{record.type}</Card.Text>
			<Card.Text>{record.amount}</Card.Text>
			<Card.Text>{record.description}</Card.Text>
			<Card.Text className="card_date">
				<Moment date={record.recordedOn} />
			</Card.Text>
			<Button  variant="danger">Remove</Button>
			</Card.Body>
		</Card>
		);
	});
	//For fetching Balance ,expense and Income
	const [compute, setCompute] = useState([]);
	useEffect(() => {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
		{
			headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		}
		)
		.then((res) => res.json())
		.then((data) => {
			setCompute(data);
		});
	}, [addedRec]);
	//for computation
	const [income, setIncome] = useState(0);
	const [expense, setExpense] = useState(0);
	const [balance, setBalance] = useState(0);
	useEffect(() => {
		let income = 0,
		expense = 0;
		compute.forEach((data) => {
		if (data.type === "Income") {
			income += data.amount;
		} else if (data.type === "Expense") {
			expense += data.amount;
		}
		});
		const balance = income - expense;
		setIncome(income);
		setExpense(expense);
		setBalance(balance);
	}, [compute, addedTransaction]);
	//Function for the Add button for category
	function addCategory(e) {
		e.preventDefault();
		fetch("https://young-springs-92350.herokuapp.com/api/users/category", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`,
		},
		body: JSON.stringify({
			id: user.id,
			name: categoryName,
			type: categoryType,
		}),
		})
		.then((res) => res.json())
		.then((data) => {
			if (data === true) {
			Swal.fire({
				icon: "success",
				title: "Added Successfully",
			});
			setCategoryName("");
			setCategoryType("");
			if (addedCategory === true) {
				setAddedCategory(false);
			} else {
				setAddedCategory(true);
			}
			} else {
			Swal.fire({
				icon: "error",
				title: "Failed to Add new Category",
				text: "Please see details",
			});
			}
		});
		setCategoryName("");
		setCategoryType("");
	}

	function addRecord(e) {
		e.preventDefault();
		fetch("https://young-springs-92350.herokuapp.com/api/users/transaction", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`,
		},
		body: JSON.stringify({
			id: localStorage.getItem("id"),
			type: type,
			category: categoryTypeRecord,
			amount: amountRecord,
			description: descriptionRecord,
		}),
		})
		.then((res) => res.json())
		.then((data) => {
			if (data === true) {
			Swal.fire({
				icon: "success",
				title: "New Record Added",
			});
			setType("");
			setCategoryTypeRecord("");
			setDescriptionRecord("");
			setAmountRecord(0);
			if (addedTransaction === true) {
				setAddedTransaction(false);
				setAddedRec(false);
			} else {
				setAddedTransaction(true);
				setAddedRec(true);
			}
			} else {
			Swal.fire({
				icon: "error",
				title: "Failed to Add Record",
				text: "Please see details",
			});
			}
		});
		setType("");
		setCategoryTypeRecord("");
		setDescriptionRecord("");
		setAmountRecord(0);
	}
	return (
		<>
		<div className="divDesign">
			<Row className="my-4">
			<Col md={12}>
			<Profile />
			</Col>
			<Col md={6}>
			<Card className="my=2 cardAdd">
				<Card.Header className="card-header"><h4>Add Category</h4></Card.Header>
				<div className="my-4 mx-4">
					<Form onSubmit={(e) => addCategory(e)}>
					<Form.Group controlId="transactionDesc">
						<Form.Label className="add-field-desc">Category Name</Form.Label>
						<Form.Control
						autoComplete="off"
						type="text"
						placeholder="Text Here"
						value={categoryName}
						onChange={(e) => setCategoryName(e.target.value)}
						required
						/>
					</Form.Group>
					<Form.Group controlId="selectionLabel">
						<Form.Label className="add-field-desc">Category Type</Form.Label>
						<Form.Control
						as="select"
						value={categoryType}
						onChange={(e) => setCategoryType(e.target.value)}
						>
						<option value="" disabled>--Category--</option>
						<option>Income</option>
						<option>Expense</option>
						</Form.Control>
					</Form.Group>
					{isActive1 ? (
						<Button className="form-button" type="submit">
						Add
						</Button>
					) : (
						<Button variant="dark" disabled>
						Add
						</Button>
					)}
					</Form>
				</div>
				</Card>
				</Col>
				<Col md={6}>	
				<Card className="my=2 cardAdd2">
				<Card.Header className="card-header"><h4>Add New Record</h4></Card.Header>
				<div className="my-4 mx-4">
					<Form onSubmit={(e) => addRecord(e)}>
					<Form.Group controlId="selectionLabel">
						<Form.Label className="add-field-desc">Category</Form.Label>
						<Form.Control
						as="select"
						value={type}
						onChange={(e) => setType(e.target.value)}
						>
						<option value="" disabled>--Filter by--</option>
						<option>Income</option>
						<option>Expense</option>
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="selectionLabel">
						<Form.Label className="add-field-desc">Category Type</Form.Label>
						<Form.Control
						as="select"
						value={categoryTypeRecord}
						onChange={(e) => setCategoryTypeRecord(e.target.value)}
						>
						<option value="" disabled></option>
						{type === "Income"
							? categoryIncomeOption
							: type === "Expense"
							? categoryExpenseOption
							: null}
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="transactionDesc">
						<Form.Label className="add-field-desc">Description</Form.Label>
						<Form.Control
						autoComplete="off"
						type="text"
						placeholder="Description"
						value={descriptionRecord}
						onChange={(e) => setDescriptionRecord(e.target.value)}
						required
						/>
					</Form.Group>
					<Form.Group controlId="transactionAmount">
						<Form.Label className="add-field-desc">Amount</Form.Label>
						<Form.Control
						type="number"
						placeholder="Amount"
						value={amountRecord}
						onChange={(e) => setAmountRecord(e.target.value)}
						required
						/>
					</Form.Group>
					{isActive2 ? (
						<Button className="form-button" type="submit">
						Add
						</Button>
					) : (
						<Button variant="dark" disabled>
						Add
						</Button>
					)}
					</Form>
				</div>
				</Card>
			</Col>
			<Col md={8}>
				<Filters/>
			</Col>
			<Col md={4}>
				<Card className="cardHighlight">
				<Card.Body>
					<h5 className="my-4 card-name">BALANCE</h5>
					<h5 id="balance" className="card-amount">
					Php {balance.toFixed(2)}
					</h5>
					<h5 className="my-4 card-name">INCOME</h5>
					<h5 id="balance" className="card-amount">
					Php {income.toFixed(2)}
					</h5>
					<h5 className="my-4 card-name">EXPENSE</h5>
					<h5 id="balance" className="card-amount">
					Php {expense.toFixed(2)}
					</h5>
				</Card.Body>
				</Card>
			</Col>
			</Row>
		</div>

		</>
	);
}
