import {Row,Col,Card}from 'react-bootstrap'
import Image from 'next/image'

export default function Transactions(){
	return (
		<>	
			<div>
				<Row className="my-4">
					<Col md={12}>
						<Card className="card-about">
							<div>
								<Image
									className="card-about-photo"
									src="/me.jpg"
									alt="Picture of the author"
									height={300}
									width={300}
								/>
							</div>
							<Card.Title>
								<h2 className="my-4 text-center ">About the Developer</h2>
							</Card.Title>
							<Card.Body>
								<p className="about-desc text-center">Hi! Everyone, Thank you for visiting my web applicaton. I am so pleased and priviledged.</p>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</div>
		</>
	)
}