import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../../userContext";
import { Row, Col, Card } from "react-bootstrap";
import { Doughnut } from "react-chartjs-2";
import Moment from "react-moment";

export default function Filters() {
  //destructure the returned value of our context
	const { user } = useContext(UserContext);
	//variable to capitalize

	//variable to get the user details
	const [userFirstName, setUserFirstName] = useState([]);
	const [userLastName, setUserLastName] = useState([]);
	//Variables for fetching options for Category and records and search

	const [records, setRecords] = useState([]);
	const [selector, setSelector] = useState("");
	const [keyword, setKeyword] = useState("");
	//Variables For adding category (useStates)
	const [categoryName, setCategoryName] = useState("");
	const [categoryType, setCategoryType] = useState("");
	//for all records
	const [allRec,setRec] = useState([]);
	//Variables For adding record (useStates)
	const [type, setType] = useState("");
	const [categoryTypeRecord, setCategoryTypeRecord] = useState("");
	const [descriptionRecord, setDescriptionRecord] = useState("");
	const [amountRecord, setAmountRecord] = useState(0);
	const [userToken, setUserToken] = useState({ userToken: null });
	//For Buttons
	const [isActive1, setIsActive1] = useState(true);
	const [isActive2, setIsActive2] = useState(true);
	//use effect for search
	const [searchAll, setSearchAll] = useState([]);
	const [filteredTrans, setFilteredTrans] = useState([]);
	const [chart, setChart] = useState([]);
	const [chartIncome, setChartIncome] = useState([]);
	const [chartExpense, setChartExpense] = useState([]);
	//for chart
	useEffect(() => {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
		{
			headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		}
		)
		.then((res) => res.json())
		.then((data) => {
			setChart(data);
			setRec(data);
		});
	}, [allRec]);
	useEffect(() => {
		setChartIncome(
		chart.map((res) => {
			let income = 0;
			chart.forEach((data) => {
			if (data.type === "Income") {
				income += parseInt(data.amount);
			}
			});
			return income;
		})
		);
	}, [chart]);
	useEffect(() => {
		setChartExpense(
		chart.map((res) => {
			let expense = 0;
			chart.forEach((data) => {
			if (data.type === "Expense") {
				expense += parseInt(data.amount);
			}
			});
			return expense;
		})
		);
	}, [chart]);
	const chartInc = chartIncome[0];
	const chartExp = chartExpense[0];
	const Dchart = {
		datasets: [
		{
			data: [chartInc, chartExp],
			backgroundColor: ["#FA7000", "#142EA1"],
		},
		],
		labels: ["Income", "Expense"],
	};
	//for search
	useEffect(() => {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
		{
			headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		}
		)
		.then((res) => res.json())
		.then((data) => {
			setSearchAll(data);
			
		});
	}, [allRec]);

	const [foundsearched, setFoundSearched] = useState([]);

	useEffect(() => {
		setFilteredTrans(
		searchAll.filter((transactions) => {
			let found = transactions.description
			.toLowerCase()
			.includes(keyword.toLowerCase());
			return found;
		})
		);
	}, [keyword, searchAll,allRec]);

	useEffect(() => {
		const searchAllRecords = filteredTrans.map((result) => {
		return (
			<Card key={result._id} className="my-3 filter-cards">
			<Card.Body>
				<Card.Title className="card-title">{result.category}</Card.Title>
				<Card.Text className="card-text">{result.type}</Card.Text>
				<Card.Text className="card-text">{result.amount}</Card.Text>
				<Card.Text className="card-text">{result.description}</Card.Text>
				<Card.Text className="card-text">
				<Moment date={result.recordedOn} />
				</Card.Text>
				<Button variant="danger">Remove</Button>
			</Card.Body>
			</Card>
		);
		});
		setFoundSearched(searchAllRecords);
	}, [filteredTrans,allRec]);
	useEffect(() => {
		fetch(
		"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
		{
			headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		}
		)
		.then((res) => res.json())
		.then((data) => {
			const allArr = [];
			const recordIncome = [];
			const recordExpense = [];
			if (selector === "Income") {
			data.filter((result) => {
				if (result.type === "Income") {
				recordIncome.push(result);
				}
			});
			setRecords(recordIncome);
			} else if (selector === "Expense") {
			data.filter((result) => {
				if (result.type === "Expense") {
				recordExpense.push(result);
				}
			});
			setRecords(recordExpense);
			} else if (selector === "All") {
			data.filter((result) => {
				allArr.push(result);
			});
			setRecords(allArr);
			}
		});
	}, [selector,allRec]);
	// added trial for dlete
	function deleteRecord(id){
		fetch(`https://young-springs-92350.herokuapp.com/api/users/allTransactions/${id}`,{
			method: "DELETE",
				headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					id:id,
				})
		})
		.then((res) => res.json())
		.then((data) => {
			console.log(data)
			window.location.reload();
			fetch(
				"https://young-springs-92350.herokuapp.com/api/users/allTransactions",
				{
					headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`,
					},
				}
			)
		})
	}
	// end here
	const allRecords = records.map((record) => {
		console.log(typeof record._id)
		return (
		<Card key={record._id} className="my-3 filter-cards">
			<Card.Body>
			<Card.Title className="card-title">{record.category}</Card.Title>
			<Card.Text className="card-text">{record.type}</Card.Text>
			<Card.Text className="card-text">{record.amount}</Card.Text>
			<Card.Text className="card-text">{record.description}</Card.Text>
			<Card.Text className="card-text">
				<Moment date={record.recordedOn} />
			</Card.Text>
			<Button onClick={()=>deleteRecord(record._id)} variant="danger">Remove</Button>
			
			</Card.Body>
		</Card>
		);
	});
	return (
		<>
		<div>
			<Row >
			<Col md={12}>
				<Doughnut data={Dchart} />
			</Col>
			</Row>
		</div>
		<div className="my-4">
			<Form>
			<h4 className="filter-desc">Filter by Details</h4>
			<Form.Group controlId="keyword">
				<Form.Control
				type="text"
				placeholder="--Input Details--"
				value={keyword}
				onChange={(e) => setKeyword(e.target.value)}
				required
				/>
			</Form.Group>
			<h4 className="filter-desc">Filter by Transaction Type</h4>
			<Form.Control
				as="select"
				value={selector}
				onChange={(e) => setSelector(e.target.value)}
			>
				<option value="" disabled>--Filter by--</option>
				<option>All</option>
				<option>Income</option>
				<option>Expense</option>
			</Form.Control>
			<div>{keyword !== "" ? foundsearched : allRecords}</div>
			</Form>
		</div>
		</>
	);
}
