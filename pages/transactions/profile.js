import { useState, useEffect, useContext } from "react";
import UserContext from "../../userContext";
import {Card } from "react-bootstrap";


export default function Transactions() {
  //destructure the returned value of our context
	const { user } = useContext(UserContext);
	//variable to get the user details
	const [userFirstName, setUserFirstName] = useState([]);
	const [userLastName, setUserLastName] = useState([]);
	const [userEmail, setUserEmail] = useState([]);
	const [userToken, setUserToken] = useState({ userToken: null });
	//use effect for user details
	useEffect(() => {
		fetch("https://young-springs-92350.herokuapp.com/api/users/details", {
		headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`,
		},
		})
		.then((res) => res.json())
		.then((data) => {
			console.log(data);
			setUserFirstName(data.firstName);
			setUserLastName(data.lastName);
			setUserEmail(data.email);
		});
	}, []);
	return (
		<>
			<Card className="profile-card">
			<Card.Body>
				<h4 className="profile-card-name">Hi, {userFirstName}</h4>
				<p className="profile-card-email">{userEmail}</p>
			</Card.Body>
			</Card>
		</>
	);
}
