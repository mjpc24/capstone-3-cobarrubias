import {Row,Col,Card}from 'react-bootstrap'

export default function Transactions(){
	return (
		<>	
			<div>
				<Row className="my-4">
					<Col md={12}>
						<Card className="cardAbout">
							<Card.Body>
								<Card.Title>
									<h2 className="my-4 text-center ">About the Application</h2>
								</Card.Title>
									<h4>Budget</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultricies leo integer malesuada nunc. Posuere morbi leo urna molestie at elementum eu. Ac auctor augue mauris augue neque gravida. Lorem mollis aliquam ut porttitor leo a. Nam at lectus urna duis convallis convallis. Diam maecenas sed enim ut sem. Egestas diam in arcu cursus euismod quis viverra nibh. Aliquam faucibus purus in massa tempor. Ullamcorper sit amet risus nullam eget felis eget nunc. Etiam non quam lacus suspendisse faucibus interdum posuere lorem ipsum. Dui sapien eget mi proin sed libero enim. Id cursus metus aliquam eleifend mi in nulla.
									</p>
									<h3 id= "income"className="text-center"></h3>
								</Card.Body>
							</Card>
						</Col>
					</Row>
			</div>
		</>
	)
}