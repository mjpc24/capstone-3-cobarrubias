import { useEffect, useState, useContext } from "react";
import { Card,Form, Button } from "react-bootstrap";
//import router from nextJS for user redirection
import Router from "next/router";
//import Swal
import Swal from "sweetalert2";
export default function Register() {
  //bind to imput to track user input in real time
  //called as 2way binding
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState(0);
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	//State for conditionally rendering the submit button
	const [isActive, setIsActive] = useState(true);
	//para mamonitor ang movement ng fields
	useEffect(() => {
		if (
		firstName !== "" &&
		lastName !== "" &&
		email !== "" &&
		mobileNo !== "" &&
		password1 !== "" &&
		password2 !== "" &&
		password1 === password2 &&
		mobileNo.length === 11
		) {
		setIsActive(true);
		} else {
		setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]);

	//this function ay para sa submit para maprevent ang pag refresh check the form onSubmit din ok?
	function registerUser(e) {
		e.preventDefault();

		//Communicate to API server
		//this fetch is for email existence
		fetch("https://young-springs-92350.herokuapp.com/api/users/email-exists", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			email: email,
		}),
		})
		.then((res) => res.json())
		.then((data) => {
			if (data === false) {
			//fetch for register
			fetch("https://young-springs-92350.herokuapp.com/api/users", {
				method: "POST",
				headers: {
				"Content-Type": "application/json",
				},
				body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1,
				}),
			})
				.then((res) => res.json())
				.then((data) => {
				if (data === true) {
					Swal.fire({
					icon: "success",
					title: "Successfully Registered.",
					text: "Thank you for registering",
					});
					Router.push("/login");
				} else {
					Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: "Please check all details provided",
					});
				}
				});
			} else {
			Swal.fire({
				icon: "error",
				title: "Registration Failed.",
				text: "Email already Registered",
			});
			}
		});

		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo(0);
		setPassword1("");
		setPassword2("");
	}

	return (
		<>
			<Card className="reg-form">
			<div className="reg-header-conatiner"><h1 className="reg-header">Register and Start being money wise!</h1></div>
				<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label className="register-label">First Name</Form.Label>
					<Form.Control
					autoComplete="off"
					type="text"
					placeholder="Enter First Name"
					value={firstName}
					onChange={(e) => setFirstName(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group controlId="userLastName">
					<Form.Label className="register-label">Last Name</Form.Label>
					<Form.Control
					autoComplete="off"
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={(e) => setLastName(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label className="register-label">Email</Form.Label>
					<Form.Control
					autoComplete="off"
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group controlId="mobileNo">
					<Form.Label className="register-label">Mobile Number</Form.Label>
					<Form.Control
					autoComplete="off"
					type="number"
					placeholder="Enter Mobile number"
					value={mobileNo}
					onChange={(e) => setMobileNo(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label className="register-label">Password</Form.Label>
					<Form.Control
					autoComplete="off"
					type="password"
					placeholder="Enter Password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label className="register-label">Confirm Password</Form.Label>
					<Form.Control
					
					autoComplete="off"
					type="password"
					placeholder="Confirm Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required
					/>
				</Form.Group>
				{isActive ? (
					<Button className="reg-button" type="submit">
					Register
					</Button>
				) : (
					<Button variant="dark" disabled>
					Register
					</Button>
				)}
				</Form>
			</Card>	
		</>
	);
}
