import Banner from '../components/Banner'
export default function Home() {
	const data={
		title: "Plan and Manage your money wisely",
		content: "Track your Income and expenses"
	}
	return(
		<>
		<Banner dataProp={data}/>
		</>
	)
}
