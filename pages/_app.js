import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
//import hooks from react
import { useState, useEffect } from "react";
//Bootstrap Components
import { Container } from "react-bootstrap";
//import Navbar/Footer in the app component
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
//import Courses in the app component
import { UserProvider } from "../userContext";
/*
	NextJS uses the app component to initialize our pages. However routing is already done by NextJS for us. Here in the App.js our component is received as a prop received in this App component.If you console log the component you will be able to see the content of the active page
*/
function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		email: null,
		id: null,
	});
	/*
		Next JS is pre-rendered.Your pages are built initilally in the server then passed nto browser. localStorage does not exist until the page/component is rendered. We are going to use useEffect to get our email and isAdmin details from our localstorage instead so as to assure that we access the localStorage only after the page/component has initially rendered
	*/
	//Start of UseEfect
	useEffect(() => {
		setUser({
		email: localStorage.getItem("email"),
		id: localStorage.getItem("id"),
		});
	}, []); //end of useEffect
	//For log out
	const unsetUser = () => {
		localStorage.clear();
		// This will return the state value to its initial value
		setUser({
		email: null,
		});
	}; //end of arroe funtion
	return (
		<>
		<UserProvider value={{ user, setUser, unsetUser }}>
			<NavBar />
			<Container>
			<Component {...pageProps} />
			</Container>
			<Footer />
		</UserProvider>
		</>
	);
}
export default MyApp;
