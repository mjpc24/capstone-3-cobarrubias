import {useContext,useEffect} from 'react'
import UserContext from '../../userContext'
import Router from 'next/router'

export default function Logout(){

	const {unsetUser} = useContext(UserContext)
	useEffect(()=>{
		unsetUser()
		//Redirect the user to log in
		Router.push('/login')
	},[])
	return null
}