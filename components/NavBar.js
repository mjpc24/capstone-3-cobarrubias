import {useContext,useState} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../userContext';

export default function NavBar(){
	const [navOpen,setNavOpen]= useState(false)
	const {user} = useContext(UserContext)
	return(
		// <Navbar className="navbar-design" bg="dark" expand="lg">
		// 	<Link href="/">
		// 		<a className="nav-link-brand">Budget ni Juan</a>
		// 	</Link>
		// 	<Navbar.Toggle aria-controls="basic-navbar-nav" />
		// 	<Navbar.Collapse id="basic-navbar-nav" align="end">
		// 		<Nav className="ml-auto nav-container" id="test-nav">
		// 			{
		// 			user.email
		// 			?
		// 				<>
		// 					<Link href="/transactions">
		// 					<a className="nav-link" role="button">Budget</a>
		// 					</Link>
		// 					<Link href="/transactions/charts">
		// 					<a className="nav-link" role="button">Charts</a>
		// 					</Link>
		// 					<Link href="/transactions/about">
		// 					<a className="nav-link" role="button">About</a>
		// 					</Link>
		// 					<Link href="/logout">
		// 					<a className="nav-link" role="button">Logout</a>
		// 					</Link>
		// 				</>
		// 			:
		// 			<>
		// 			<Link href="/login">
		// 			<a className="nav-link" role="button">Login</a>
		// 			</Link>
		// 			</>
		// 			}
		// 		</Nav>
		// 	</Navbar.Collapse>
		// </Navbar>
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark navbar-design">
	<a class="nav-link-brand" href="/">Budget ni Juan</a>
		<button className="navbar-toggler" data-toggle="collapse" data-target="#link-list">
			<span className="navbar-toggler-icon"></span>
		</button>	
		<div className="collapse navbar-collapse list-link" id="link-list" >
            {
                user.email
                ?
                    <ul class="navbar-nav ml-auto list-link" >
                        <li>
                        <a href="/transactions" className="nav-link" role="button">Budget</a>
                        </li>
                        <li>
                        <a href="/transactions/chart" className="nav-link" role="button">Charts</a>
                        </li>
                        <li>
                        <a href="/transactions/about" className="nav-link" role="button">About</a>
                        </li>
                        <li>
                        <a href="/logout" className="nav-link" role="button">Logout</a>
                        </li>
                    </ul>
                :
                <ul className="navbar-nav ml-auto list-link" >
                    <li>
                    <a href="/login" className="nav-link" role="button">Login</a>
                    </li>
                </ul>
                }
		</div>
		
	</nav>
	)
}