import { Card, Row, Col, Container } from "react-bootstrap";

export default function Banner({ dataProp }) {
	const { title, content, destination, label } = dataProp;
	/*
	Link component is used in NextJS to navigate hrough pages. It uses NextJS routing and can wrap an anchor tag. NextJS link component has an href attribute which acts just like our anchor tag href attribute. However, the child <a> tag of the link component is where any styling is applied
	*/
	return (
		<Row className="my-7 container-form">
				<Col md={12} className="container-form">
					<Card className="home-logo" >
					</Card>
				</Col>
				<Col md={12}>
					<h1 className="home-desc-brand">Budget ni Juan</h1>
					<h4 className="home-desc">{title}</h4>
					<h5 className="home-desc" >{content}</h5>
				
				</Col>
			
		</Row>
	);
}
