import {useContext} from 'react'
import UserContext from '../userContext'

export default function Footer(){
	const {user} = useContext(UserContext)
	return(
		<div className="fixed-bottom text-center footer-style">
			<p className="footer-note">© Copyright Reserved | All Rights Reserved</p>
		</div>
	)
}