//This helper function which will randomize a hex color for each slice in the piechart

const colorRandomizer = ()=> {


	//It creates a random hex decimal
	//random() - returns a random number
	//it will be multiplied by 16777215 - which is the decimal designation for white
	//console.log(Math.random()) Math.random() returns a number between 0 and 1
	//Math.floor() rounds off the number to the closest integer that is the largest equal or less than the number
	//console.log(Math.floor(5.95)) =5
	//.toString = to turn the number into a string. Adding an argument like 16 into the toString method, creates a hex decimal
	//num.toString(16) = hex decimal
	return Math.floor(Math.random()*16777215).toString(16)
}

export {colorRandomizer}